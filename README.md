# README #

This is the software of a puzzle I have created, similar to Rubik's cube but kind of different.
**You can play the puzzle on Windows by downloading "Sphere.zip" in the download section.** After extracting the contents, run the file sphere.exe.

Copyright (c) 2015 Lucas Preischadt Pinheiro

# COMPILING #

gcc -Wall -o sphere sphere.c -lglut -lGL -lGLU -lm